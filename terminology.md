## Terminology 

- working folder : where you edit files. Git does not control this.
- repository : a collection of files git cares about. 
- index : where you record what files go in the next commit
- hash : a unique signature for a commit
- commit : a set of changes to files that modifies a repository
- HEAD : the latest commit in the current branch
- branch : a separate universe of files to experiment with
- merge
- fork
- bare repo
- pull request

- origin 
- upstream
