How to Git into Trouble
========================

aka Conceptual git for beginners

Paul Nijjar (hire me?)

http://pnijjar.freeshell.org

2019-11-04













## A Conversation 

Me: Hey Andrew! Add these links to <https://wrdashboard.ca>!

Andrew: What? Add them yourself.

Me: sudo Hey Andrew! Add these links to <https://wrdashboard.ca>!

Andrew: Paul is not in the sudoers list. This incident will be
reported.

Me: How do I get these links added to <https://wrdashboard.ca>??

Andrew: Do a merge request!

Me: ... 

Me: Okay, I guess. I'll learn how to make a merge request.

Me: HOW HARD COULD IT BE?










## Version Control 

### Ideas

- Keep track of changes you made to files.
- Keep track of changes OTHER people make to your files.
- Branch your files into alternative universes to try new things or
  develop dangerous features.

Version control is usually associated with software source code, 
but it does not have to be. 

There are many version control systems, but git won.









### WHY???

- Go back and find where you introduced bugs
- Allow many people to work on the same set of files simultaneously
- Reduce worries about "killing your babies"
- Roll back stupid ideas when necessary
- Support many different versions/releases of your files at once







## Goals

- Learn enough git/Gitlab commands to make a merge request

- Learn enough about how git views the world to work with it. 





  
  
## Caveats

- I am a git beginner (really) 

- I am intentionally glossing over some details

- I am focusing on Gitlab, but most knowledge will transfer to Github
  and other systems











## Prerequisites

- You are okay with using the commandline
  + navigating folders
  + typing commands
  + using a text editor
  + using a bit of SSH

- You have made a Github/Gitlab account

- You have associated an SSH key with that account, and have used
  `ssh-agent` to remember that key















## The funniest joke

Q: How do you make a bajillion dollars on the internet?

A: 
















## Demo Time 

(Rats. Forgot a demo. Let's wing it...)






## Our Own Repository, on Our Own Laptop

### Before git

~~~
git status

fatal: Not a git repository (or any of the parent directories): .git
~~

We have to tell git that we want a repository for our files.

repository: a collection of files/folders git tracks. 


### git init 

Tell git we want a directory.

~~~

git init
git status


On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        pix/
        talk.md

nothing added to commit but untracked files present (use "git add" to track)

~~~

"branch master" is the default "branch" git creates.

Nothing is being tracked by git yet.

Git creates a folder `.git` and an "index" for staging files to be tracked.

Let's add some files.


### Adding files

~~~

git add talks.md
git add terminology.md

git status



~~~

Now there are files in the index, but they have not been committed.

Git cares about changesets (to one or many files), not about individual files itself.

Git takes a "snapshot" of the files at the time we add them. 

Let's commit these changes.


### git commit 

~~~ 
git commit -m "Documentation until first commit"

git commit -m "Just before committing"
[master (root-commit) 171930d] Just before committing
 2 files changed, 188 insertions(+)
 create mode 100644 talk.md
 create mode 100644 terminology.md

git status

~~~

This makes a "commit". The commit has an ID (called a hash) which is supposed to be unique for this commit. 

~~~
git log 

commit 171930d7e4d595296f59365e40cf197452c75b74
Author: Paul Nijjar <pnijjar@alumni.uwaterloo.ca>
Date:   Sun Nov 3 22:02:22 2019 -0500

    Just before committing

git log -p



git log --pretty=oneline

git log --name-only
~~~

The unique ID for this commit is `171930d7e4d595296f59365e40cf197452c75b74 `

We can use a short form (usually 6 characters) so long as it is not ambiguous. 

The latest commit gets the name `HEAD`

### Adding more 

~~~ 
git status
git add pix
git add talks.md

~~~

By default, every time you change files you need to re-add them.

If you add a file, change it, and do `git status` then things look
weird, because git takes a snapshot.

Every commit knows about its parent.

To see the changes: 

~~~
git diff

git diff talks.md
~~~


## Branching and Merging

Say we want to make a dramatic change (actually not) that is dangerous
in production. We can make a new branch (version of the world).

~~~
git branch -a
git branch terminology
git branch -a
git checkout terminology
git checkout master
~~~

The working directory stays the same! So does the index, which is
confusing!

Git tries hard not to clobber files in your working folder, which 
gets irritating.

Let's log changes to `talks.md` in master and changes to
`terminology.md` in terminology.

~~~
git checkout master
git add pix/02-*
git add pix/03-*
git add terminology.md
git commit
~~~

In terminology

~~~
git checkout terminology
git add pix/04*
git add terminology.md
git commit
~~~


### Merging

Once we are happy with the changes in the new branch, we can 
(try) to bring them back together

~~~
git checkout master
git merge terminology 
~~~

Now we hope for the best. Maybe we need to fix conflicts

This makes a NEW commit in master that is the result of the merge. 

After you can delete the branch (which still exists) if you want.

~~~
git branch -d terminology
~~~


## Working with Gitlab

Start a new Gitlab project. Then set the "remote" from our folder to
Gitlab. Then "push" our changes up. 






